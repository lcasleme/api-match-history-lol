﻿using System.Collections.Generic;

namespace Domain.DTO
{
    public class MatchlistDTO
    {
        public int StartIndex { get; set; }

        /// <summary>
        /// There is a known issue that this field doesn't correctly return the total number of games that match the parameters of the request. Please paginate using beginIndex until you reach the end of a player's matchlist.
        /// </summary>
        public int TotalGames { get; set; }

        public int EndIndex { get; set; }

        /// <summary>
        /// List<MatchReferenceDto>
        /// </summary>
        public List<MatchReferenceDTO> Matches { get; set; }
    }
}